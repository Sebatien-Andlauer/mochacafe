# Mochacafé

 Intégration HTML / CSS / Javascript Mocha Café
 Réalisation d'une maquette responsive pour le test MochaCafé 
 Restriction : - Respect des tailles et positions des éléments
               - Respect des marges
               - Respect des polices d'écriture
 Restriction de programmation Responsive : Ecrans de 320 px pour le plus petit jusqu'aux écrans les plus grands.  